import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(Root());
}

class Root extends StatelessWidget {
  Root({Key? key}) : super(key: key);

  void onTap() async {
    bool isNFCAvailable = await NfcManager.instance.isAvailable();
    if (isNFCAvailable) {
      //
    } else {}
    NfcManager.instance.startSession(
      onDiscovered: (NfcTag tag) async {
        Ndef? ndef = Ndef.from(tag);
        if (ndef != null) {
          if (ndef.cachedMessage?.records.isNotEmpty ?? false) {}
        }
      },
    ).onError((error, stackTrace) {
      log("GOT ERROR");
    });

    log("STOPPING SESSION");
    log("SESSOPM STOPPED");
  }

  Future<void> _readnfcCard() async {
    log("reading nfc tag");
    NfcManager.instance.stopSession(errorMessage: 'STOPPING EXISTING INSTANCE');
    await NfcManager.instance.startSession(
      onDiscovered: (NfcTag nfcTag) async {
        log(nfcTag.handle.toString());
        log(nfcTag.data.toString());
        Ndef? ndef = Ndef.from(nfcTag);
        log("GOT NFC CARD");
        log(ndef?.additionalData.toString() ?? "NA");
        log(ndef?.toString() ?? "NA");
        if (ndef?.cachedMessage?.records.isNotEmpty ?? false) {
          log("TRUE");
          for (NdefRecord record in ndef!.cachedMessage!.records) {
            String writtenMessage = String.fromCharCodes(record.payload);
            log(writtenMessage);
          }
          // log(ndef?.additionalData.toString() ?? "NA");
          // log(ndef?.cachedMessage?.records.first.payload.toString() ?? "NA");
          // String encryptedMessage =
          // String.fromCharCodes(ndef!.cachedMessage!.records.first.payload);
          // log(encryptedMessage.replaceAll(" ", ""));
          // String message = _getDecryptedString(
          //     inputBase64String: encryptedMessage.substring(1));
          //NOTE : Using substring here to get rid of white splce (or some special character maybe).
          // log("MESSAGE : $encryptedMessage");
        }
      },
    );
  }

  void _writeInNfcCard({required String inputMessage}) async {
    log("WRITING");

    NfcManager.instance.stopSession(errorMessage: 'STOPPING EXISTING INSTANCE');
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      Ndef? ndef = Ndef.from(tag);
      log(ndef.toString());
      log(ndef?.isWritable.toString() ?? "NA");
      if (ndef == null || !ndef.isWritable) {
        log("NDEF NULL");
        _writeInNfcCard(inputMessage: inputMessage);
        return;
      }

      tag.data.clear();

      // String encryptedMessage = _getEncryptedString(
      //   fromDate: "2023-03-01",
      //   toDate: "2023-04-01",
      //   placeId: "106",
      // );
      // String
      NdefMessage message = NdefMessage([
        NdefRecord.createText(
          inputMessage,
          languageCode: "",
        ),
        NdefRecord.createMime(
          'text/plain',
          Uint8List.fromList(
            inputMessage.codeUnits,
          ),
        ),
        // NdefRecord.createMime(
        //   'text/plain',
        //   Uint8List.fromList(
        //     'Hello'.codeUnits,
        //   ),
        // ),
      ]);

      try {
        log("WRITING MESSAGE");
        await ndef.write(message);
        log("WRITING DONE");
        // NfcManager.instance.stopSession();
      } catch (e) {
        log("ERROR IN WRITING MESSAGE");
        log(e.toString());
        _writeInNfcCard(inputMessage: inputMessage);
        return;
      }
    });
  }

  void _closeNfcConnection() {
    NfcManager.instance.stopSession(
      alertMessage: "ENDING SESSION",
    );
  }

  static const myChannel = MethodChannel('example.com/device');

  void displayNativeToast() {
    print("DISPLAYING TOAST");
    myChannel.invokeMethod("callToast");
  }

  void doAddition() async {
    print("DOING ADDITION");
    var arguments = {
      'number1': 3,
      'number2': 4,
    };
    int res = await myChannel.invokeMethod("addition", arguments);
    print(res);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text("flutter Nfc Demo".toUpperCase()),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("FLUTTER NFC DEMO APP"),
              ElevatedButton(
                onPressed: doAddition,
                child: const Text("SHOW TOAST"),
              ),
              ElevatedButton(
                onPressed: _readnfcCard,
                child: const Text("READ NFC"),
              ),
              ElevatedButton(
                onPressed: () =>
                    _writeInNfcCard(inputMessage: "Hello World Again"),
                child: const Text("WRITE NFC"),
              ),
              ElevatedButton(
                onPressed: _closeNfcConnection,
                child: const Text("End NFC CONNECTION"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
