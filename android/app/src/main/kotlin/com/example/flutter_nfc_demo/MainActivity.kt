package com.example.flutter_nfc_demo


import io.flutter.embedding.android.FlutterActivity
import android.widget.Toast
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel


class MainActivity : FlutterActivity() {

//    private static final String channel = "device"
//
//    fun onMethodCall(call:MethodCall,result Result){
//        when(call.method){
//            "callToast" -> showToastFromkt()
//        }
//    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            "example.com/device"
        ).setMethodCallHandler { call, result ->
            when(call.method){
                "callToast" -> showToastFromkt()
                "addition" -> doAddition(call, result)
            }

        }
    }


    private fun showToastFromkt() {
        Toast.makeText(applicationContext, "this is toast message", Toast.LENGTH_SHORT).show()
    }
    
    private fun doAddition(call: MethodCall, result: MethodChannel.Result){
        val number1:Int = call.argument("number1") ?: 0
        val number2:Int = call.argument("number2") ?: 0
        
        result.success(number1 + number2)
    }

}
